﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FilmsCatalog.Models
{
    public class GenreShortViewModel
    {
        [UIHint("HiddenInput")]
        public Guid Id { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }
    }
}
