﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FilmsCatalog.Models
{
    public class FilmCreateEditViewModel
    {
        [UIHint("HiddenInput")]
        public Guid Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Не указано название фильма")]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 150 символов")]
        public string Title { get; set; }


        [Display(Name = "Краткое описание")]
        [DataType(DataType.MultilineText)]        
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 255 символов")]
        public string Annotation { get; set; }


        [Display(Name = "Полное описание")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Не указано описание фильма")]
        [StringLength(5000, MinimumLength = 5, ErrorMessage = "Длина строки должна быть от 5 до 5000 символов")]
        public string Description { get; set; }


        [Display(Name = "Год выхода на экран")]
        [Range(1900, 2021, ErrorMessage = "Недопустимый год")]
        [Required(ErrorMessage = "Не указан год выхода фильма на экран ")]
        public int Year { get; set; }


        [Display(Name = "Режиссер")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string Director { get; set; }


        [Display(Name = "Длительность")]
        [DataType(DataType.Duration)]
        public TimeSpan? Duration { get; set; }


        [Display(Name = "Постер")]
        [DataType(DataType.Upload)]
        public string PosterImagePath { get; set; }

        [Display(Name = "Трейлер")]
        [Url(ErrorMessage = "Должен быть указан допустимый url адрес")]
        [DataType(DataType.Url)]
        public string TrailerUrl { get; set; }

        [Display(Name = "ЧПУ")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 100 символов")]
        public string Slug { get; set; }


        [Display(Name = "SEO метатег Title")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 100 символов")]
        public string MetaTitle { get; set; }


        [Display(Name = "SEO метатег Description")]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 150 символов")]
        public string MetaDescription { get; set; }


        [Display(Name = "SEO метатег Keywords")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 100 символов")]
        public string MetaKeywords { get; set; }

        [Display(Name = "Жанры")]
        public virtual List<GenreShortViewModel> Genres { get; set; }

        public string AutherId { get; set; }
    }
}
