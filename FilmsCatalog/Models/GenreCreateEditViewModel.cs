﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FilmsCatalog.Models
{
    public class GenreCreateEditViewModel
    {
        [UIHint("HiddenInput")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Не указано название")]
        [Display(Name = "Название")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 100 символов")]
        public string Name { get; set; }       
    }
}
