﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FilmsCatalog.Models
{
    public class FilmShortViewModel
    {
        [UIHint("HiddenInput")]
        public Guid Id { get; set; }

        [Display(Name = "Название")]
        public string Title { get; set; }

        [Display(Name = "Краткое описание")]
        public string Annotation { get; set; }

        [Display(Name = "Год выхода на экран")]
        public int Year { get; set; }     

        [Display(Name = "Постер")]       
        public string PosterImagePath { get; set; }

        [Display(Name = "Жанры")]
        public virtual List<GenreShortViewModel> Genres { get; set; }

        public string AutherId { get; set; }

    }
}
