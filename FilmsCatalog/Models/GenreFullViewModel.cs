﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FilmsCatalog.Models
{
    public class GenreFullViewModel
    {
        [UIHint("HiddenInput")]
        public Guid Id { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }
        public virtual List<FilmShortViewModel> Films { get; set; }
    }
}
