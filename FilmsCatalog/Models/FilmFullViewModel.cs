﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FilmsCatalog.Models
{
    public class FilmFullViewModel
    {
        [UIHint("HiddenInput")]
        public Guid Id { get; set; }


        [Display(Name = "Название")]
        public string Title { get; set; }

        [Display(Name = "Содержание")]
        public string Description { get; set; }

        [Display(Name = "Год выхода на экран")]
        public int Year { get; set; }


        [Display(Name = "Режиссер")]
        public string Director { get; set; }

        [Display(Name = "Длительность, мин")]
        [DataType(DataType.Duration)]
        public TimeSpan? Duration { get; set; }


        [Display(Name = "Постер")]       
        public string PosterImagePath { get; set; }


        [Display(Name = "Трейлер")]        
        public string TrailerUrl { get; set; }
                    

        [Display(Name = "SEO метатег Title")]       
        public string MetaTitle { get; set; }


        [Display(Name = "SEO метатег Description")]       
        public string MetaDescription { get; set; }


        [Display(Name = "SEO метатег Keywords")]      
        public string MetaKeywords { get; set; }

        [Display(Name = "Жанры")]
        public virtual List<GenreShortViewModel> Genres { get; set; }

        public string AutherId { get; set; }
    }
}
