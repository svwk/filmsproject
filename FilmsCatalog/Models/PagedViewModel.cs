﻿using System;

namespace FilmsCatalog.Models
{
    public class PagedViewModel<T>
    {
        public T Data {get; set;}
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public int TotalPages { get; set; }
        public int TotalRecords { get; set; }

        public PagedViewModel(T data, int pageNumber, int pageSize, int totalRecords)
        {
            Data = data;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            TotalRecords = totalRecords;
            TotalPages = (int)Math.Ceiling(TotalRecords / (double)pageSize);          
        }

        public bool HasPreviousPage
        {
            get
            {
                return (PageNumber > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageNumber < TotalPages);
            }
        }
    }
}
