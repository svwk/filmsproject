using System;
using System.Security.Claims;
using System.Threading.Tasks;
using FilmsCatalog.Data;
using FilmsCatalog.Data.Repositories;
using FilmsCatalog.Domain.Models;
using FilmsCatalog.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace FilmsCatalog.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly HttpContext _httpContext;        
        protected DbSet<User> _applicationUsers { get; }


        public CurrentUserService(IHttpContextAccessor httpContextAccessor, ApplicationDbContext context)
        {
            if (httpContextAccessor != null)
            {
                _httpContext = httpContextAccessor.HttpContext;
                if (_httpContext != null && _httpContext.User.Identity != null)
                {
                    IsAuthenticated = _httpContext.User.Identity.IsAuthenticated;
                    if (IsAuthenticated)
                    {
                        UserId = _httpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);                       
                        Email = _httpContext.User.FindFirstValue(ClaimTypes.Email);
                        Role = _httpContext.User.FindFirstValue(ClaimTypes.Role);
                        Name = _httpContext.User.FindFirstValue(ClaimTypes.Name);
                    }
                    else
                    {

                    }
                    _applicationUsers = context.ApplicationUsers;
                }
            }
        }

        public string UserId { get; } //=  "3b62472e-4f66-49fa-a20f-e7685b9565d8";

        public bool IsAuthenticated { get; } = false;

        public string Email { get; } 

        public string Name { get; } 

        public string Role { get; } 


        public async Task<bool> CheckUserAsync(string userId)
        {
            if (String.IsNullOrWhiteSpace(userId)) return false;
           return await _applicationUsers.AnyAsync(e => e.Id == userId);           
        }
    }
}