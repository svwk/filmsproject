﻿using AutoMapper;
using FilmsCatalog.Domain.Interfaces;
using FilmsCatalog.Domain.Models;
using FilmsCatalog.Extensions;
using FilmsCatalog.Models;
using FilmsCatalog.Services.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FilmsCatalog.Services
{
    public class FilmService : IFilmService
    {
        private readonly IFilmRepository _filmRepository;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;
        private IMemoryCache cache;

        public FilmService(IFilmRepository filmRepository, ICurrentUserService currentUserService, IMapper mapper, IMemoryCache memoryCache)
        {
            _filmRepository = filmRepository;
            _currentUserService = currentUserService;
            _mapper = mapper;
            cache = memoryCache;
        }

        public async Task<ICollection<FilmShortViewModel>> GetAllFilmsAsync(int pageNumber, int pageSize)
        {
            var films = await _filmRepository.GetPagedAsync(pageNumber, pageSize);

            return _mapper.Map<ICollection<FilmShortViewModel>>(films);
        }

        public async Task<ICollection<FilmShortViewModel>> GetFilmsByUserIdAsync(string userId, int pageNumber, int pageSize)
        {
            var films = await _filmRepository.GetPagedByUserIdAsync(userId, pageNumber, pageSize);

            return _mapper.Map<ICollection<FilmShortViewModel>>(films);
        }


        public async Task<ICollection<FilmShortViewModel>> GetTopFilmsAsync()
        {
            var films = await _filmRepository.GetTopAsync(6);

            return _mapper.Map<ICollection<FilmShortViewModel>>(films);
        }

        public async Task<FilmFullViewModel> GetFilmByIdAsync(Guid Id)
        {
            var film = await _filmRepository.GetByIdAsync(Id);

            if (film == null)
            {
                throw new AppException($"Film with {Id} wasn't found", 500);
            }

            return _mapper.Map<FilmFullViewModel>(film);
        }

        public async Task<FilmShortViewModel> GetShortFilmByIdAsync(Guid Id)
        {
            var film = await _filmRepository.GetByIdAsync(Id);

            if (film == null)
            {
                throw new AppException($"Film with {Id} wasn't found", 500);
            }

            return _mapper.Map<FilmShortViewModel>(film);
        }

        public async Task<FilmCreateEditViewModel> GetForEditFilmByIdAsync(Guid Id)
        {
            var film = await _filmRepository.GetByIdAsync(Id);

            if (film == null)
            {
                throw new AppException($"Film with {Id} wasn't found", 500);
            }

            return _mapper.Map<FilmCreateEditViewModel>(film);
        }

        public async Task<Guid> AddNewFilmAsync(FilmCreateEditViewModel film, string userid)
        {
            if (film is null)  throw new AppException($"Request cannot be empty", 400);            

            if (!await _currentUserService.CheckUserAsync(userid)) throw new AppException($"Access fobidden", 401);

            var createFilm = _mapper.Map<Film>(film);
            createFilm.AutherId = userid;

            var newId = await _filmRepository.CreateAsync(createFilm);

            return newId;
        }

        public async Task<string> UpdateFilmAsync(Guid id, FilmCreateEditViewModel newfilm, string userid)
        {
            if (!await _currentUserService.CheckUserAsync(userid)) throw new AppException($"Access fobidden", 401);
            if (newfilm is null) throw new AppException($"Request cannot be empty", 400);

            Film oldFilm = await _filmRepository.GetByIdAsync(id);

            if (oldFilm == null)
            {
                throw new AppException($"Film with {id} wasn't found", 500);
            }
            
            Film updatedFilm = _mapper.Map<FilmCreateEditViewModel, Film>(newfilm, oldFilm);

            await _filmRepository.UpdateAsync(updatedFilm);

            return $"Film with {id} was succefully updated";
        }

        public async Task<string> RemoveFilmAsync(Guid id, string userid)
        {
            if (!await _currentUserService.CheckUserAsync(userid)) throw new AppException($"Access fobidden", 401);
            var Film = await _filmRepository.GetByIdAsync(id);

            if (Film == null)
            {
                throw new AppException($"Film with {id} wasn't found", 500);
            }

            await _filmRepository.DeleteAsync(Film);

            return $"Film with {id} was succefully removed";
        }

        public async Task<bool> ExistsAsync(Guid id)
        {
            return await _filmRepository.AnyAsync(id);            
        }

        public async Task<int> FilmsCount()
        {
            int count = 0;
            
            if (!cache.TryGetValue("FilmsCount", out count))
            {
                count = await _filmRepository.Count();       
                cache.Set("FilmsCount", count, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(5)));                
            }
            return count;

        }
        public async Task<int> FilmsCountByUserId(string userid)
        {
            int count = await _filmRepository.CountByUserId(userid);            
          
            return count;

        }
    }
}