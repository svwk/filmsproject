﻿using AutoMapper;
using FilmsCatalog.Domain.Interfaces;
using FilmsCatalog.Domain.Models;
using FilmsCatalog.Extensions;
using FilmsCatalog.Models;
using FilmsCatalog.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FilmsCatalog.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _GenreRepository;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;

        public GenreService(IGenreRepository GenreRepository, ICurrentUserService currentUserService, IMapper mapper)
        {
            _GenreRepository = GenreRepository;
            _currentUserService = currentUserService;
            _mapper = mapper;           
        }

        public async Task<ICollection<GenreShortViewModel>> GetAllGenresAsync()
        {
            var Genres = await _GenreRepository.GetAllAsync();

            return _mapper.Map<ICollection<GenreShortViewModel>>(Genres);
        }

        public async Task<GenreFullViewModel> GetGenreByIdAsync(Guid Id)
        {
            var Genre = await _GenreRepository.GetByIdAsync(Id);

            if (Genre == null)
            {
                throw new AppException($"Genre with {Id} wasn't found", 500);
            }

            return _mapper.Map<GenreFullViewModel>(Genre);
        }

        public async Task<GenreShortViewModel> GetShortGenreByIdAsync(Guid Id)
        {
            var Genre = await _GenreRepository.GetByIdAsync(Id);

            if (Genre == null)
            {
                throw new AppException($"Genre with {Id} wasn't found", 500);
            }

            return _mapper.Map<GenreShortViewModel>(Genre);
        }

        public async Task<GenreCreateEditViewModel> GetForEditGenreByIdAsync(Guid Id)
        {
            var Genre = await _GenreRepository.GetByIdAsync(Id);

            if (Genre == null)
            {
                throw new AppException($"Genre with {Id} wasn't found", 500);
            }

            return _mapper.Map<GenreCreateEditViewModel>(Genre);
        }

        public async Task<Guid> AddNewGenreAsync(GenreCreateEditViewModel Genre, string userid)
        {
            if (Genre is null)  throw new AppException($"Request cannot be empty", 400);            

            if (!await _currentUserService.CheckUserAsync(userid)) throw new AppException($"Access fobidden", 401);

            var createGenre = _mapper.Map<Genre>(Genre);

            var newId = await _GenreRepository.CreateAsync(createGenre);

            return newId;
        }

        public async Task<string> UpdateGenreAsync(Guid id, GenreCreateEditViewModel newGenre, string userid)
        {
            if (!await _currentUserService.CheckUserAsync(userid)) throw new AppException($"Access fobidden", 401);
            if (newGenre is null) throw new AppException($"Request cannot be empty", 400);

            Genre oldGenre = await _GenreRepository.GetByIdAsync(id);

            if (oldGenre == null)
            {
                throw new AppException($"Genre with {id} wasn't found", 500);
            }

            Genre updatedGenre = _mapper.Map<GenreCreateEditViewModel, Genre>(newGenre, oldGenre);

            await _GenreRepository.UpdateAsync(updatedGenre);

            return $"Genre with {id} was succefully updated";
        }

        public async Task<string> RemoveGenreAsync(Guid id, string userid)
        {
            if (!await _currentUserService.CheckUserAsync(userid)) throw new AppException($"Access fobidden", 401);
            var Genre = await _GenreRepository.GetByIdAsync(id);

            if (Genre == null)
            {
                throw new AppException($"Genre with {id} wasn't found", 500);
            }

            await _GenreRepository.DeleteAsync(Genre);

            return $"Genre with {id} was succefully removed";
        }

        public async Task<bool> ExistsAsync(Guid id)
        {
            return await _GenreRepository.AnyAsync(id);            
        }
    }
}