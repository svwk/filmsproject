﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FilmsCatalog.Models;

namespace FilmsCatalog.Services.Interfaces
{
    public interface IGenreService
    {
        Task<ICollection<GenreShortViewModel>> GetAllGenresAsync();

        Task<GenreFullViewModel> GetGenreByIdAsync(Guid id);

        Task<GenreShortViewModel> GetShortGenreByIdAsync(Guid Id);

        Task<GenreCreateEditViewModel> GetForEditGenreByIdAsync(Guid Id);

        Task<Guid> AddNewGenreAsync(GenreCreateEditViewModel Genre, string userid);

        Task<string> UpdateGenreAsync(Guid id, GenreCreateEditViewModel Genre, string userid);

        Task<string> RemoveGenreAsync(Guid id, string userid);

        Task<bool> ExistsAsync(Guid id);
    }
}