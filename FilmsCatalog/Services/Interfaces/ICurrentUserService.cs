using System;
using System.Threading.Tasks;

namespace FilmsCatalog.Services.Interfaces
{
    public interface ICurrentUserService
    {
        string UserId { get; }
        bool IsAuthenticated { get; }
        string Email { get; }
        string Name { get; }
        string Role { get; }

        Task<bool> CheckUserAsync(string userId);
    }
}