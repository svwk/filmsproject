﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FilmsCatalog.Models;

namespace FilmsCatalog.Services.Interfaces
{
    public interface IFilmService
    {
        Task<ICollection<FilmShortViewModel>> GetAllFilmsAsync(int pageNumber, int pageSize);

        Task<ICollection<FilmShortViewModel>> GetFilmsByUserIdAsync(string userId, int pageNumber, int pageSize);

        Task<ICollection<FilmShortViewModel>> GetTopFilmsAsync();

        Task<FilmFullViewModel> GetFilmByIdAsync(Guid id);

        Task<FilmShortViewModel> GetShortFilmByIdAsync(Guid Id);

        Task<FilmCreateEditViewModel> GetForEditFilmByIdAsync(Guid Id);

        Task<Guid> AddNewFilmAsync(FilmCreateEditViewModel film, string userid);

        Task<string> UpdateFilmAsync(Guid id, FilmCreateEditViewModel film, string userid);

        Task<string> RemoveFilmAsync(Guid id, string userid);

        Task<bool> ExistsAsync(Guid id);

        Task<int> FilmsCount();

        Task<int> FilmsCountByUserId(string userid);
    }
}