﻿using AutoMapper;
using FilmsCatalog.Domain.Models;
using FilmsCatalog.Models;

namespace FilmsCatalog.Extensions
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Film, FilmCreateEditViewModel>();
            CreateMap<FilmCreateEditViewModel, Film>();
            CreateMap<Film, FilmFullViewModel>();
            CreateMap<Film, FilmShortViewModel>();
            CreateMap<Genre, GenreCreateEditViewModel>();
            CreateMap<GenreCreateEditViewModel, Genre>();
            CreateMap<Genre, GenreFullViewModel>();
            CreateMap<Genre, GenreShortViewModel>();

        }
    }
}
