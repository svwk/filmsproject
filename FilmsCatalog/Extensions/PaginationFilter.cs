﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace FilmsCatalog.Extensions
{
    public class PaginationFilter
    {
        [BindRequired]
        public int PageNumber { get; set; }

        [BindRequired]
        public int PageSize { get; set; }
        public PaginationFilter()
        {
            PageNumber = 1;
            PageSize = 5;
        }
        public PaginationFilter(int pageNumber, int pageSize)
        {
            PageNumber = pageNumber < 1 ? 1 : pageNumber;
            PageSize = pageSize > 10 ? 10 : pageSize;
        }
    }
}
