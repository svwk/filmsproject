﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FilmsCatalog.Data;
using FilmsCatalog.Services.Interfaces;
using FilmsCatalog.Models;
using FilmsCatalog.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.Extensions.Logging;

namespace FilmsCatalog.Controllers
{
    [Route("films")]
    public class FilmsController : Controller
    {       
        private readonly IFilmService _filmService;
        private readonly IGenreService _genreService;        
        private readonly  IWebHostEnvironment _appEnvironment;
        private readonly ILogger<AccountController> _logger;

        public FilmsController(
            ApplicationDbContext context,            
            IFilmService filmService,
            IGenreService genreService,
            IWebHostEnvironment appEnvironment,
            ILogger<AccountController> logger)
        {                   
            _filmService = filmService;
            _genreService = genreService;
            _appEnvironment = appEnvironment;
            _logger = logger;
        }

        // GET: Films
        [HttpGet("{pageSize:int}/{pageNumber:int}",Name ="filmCatalog")]
        public async Task<IActionResult> Index(PaginationFilter filter)
        {            
            var films = await _filmService.GetAllFilmsAsync(filter.PageNumber, filter.PageSize);
            var total = await _filmService.FilmsCount();
            var page = new PagedViewModel<IEnumerable<FilmsCatalog.Models.FilmShortViewModel>>(films, filter.PageNumber, filter.PageSize, total);
            
            return View("index", page);
        }
        

        [Authorize]
        [HttpGet("list/{pageSize:int}/{pageNumber:int}", Name = "filmList")]
        public async Task<IActionResult> List(PaginationFilter filter)
        {        
            
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId == null) return Forbid();

            var films = await _filmService.GetFilmsByUserIdAsync(userId, filter.PageNumber, filter.PageSize);
            var total = await _filmService.FilmsCountByUserId(userId);
            var page = new PagedViewModel<IEnumerable<FilmsCatalog.Models.FilmShortViewModel>>(films, filter.PageNumber, filter.PageSize, total);
            return View("list", page);
        }

        // GET: Films/Details/5
        [HttpGet("{id:guid}", Name ="filmDetails")]
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var film = await _filmService.GetFilmByIdAsync(id.Value);            
            if (film == null) return NotFound();           

            return View(film);
        }


        // GET: Films/Create
        [Authorize]
        [HttpGet("create",Name ="filmCreate")]
        public IActionResult Create()
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);           
            if (userId == null) return Forbid();

            ViewData["AutherId"] = userId;           
            return View();
        }

        // POST: Films/Create       
        [HttpPost("create", Name ="filmCreatePost")]
        [Authorize]
        [ValidateAntiForgeryToken]        
        public async Task<IActionResult> Create(FilmCreateEditViewModel newFilm)
        {                      
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId == null) return Forbid();
            if (newFilm == null) return BadRequest(); 

            if (ModelState.IsValid)
            {               
               var result = await _filmService.AddNewFilmAsync(newFilm, userId);
                _logger.LogInformation($"Film with id={newFilm.Id} was succesfully created");
                return RedirectToAction(nameof(Index), new { pageSize = 5, pageNumber = 1 }); ;
            }
           
            ViewData["AutherId"] = userId;
            return View(newFilm);
        }

        // GET: Films/Edit/5
        [Authorize]
        [HttpGet("edit/{id:guid}", Name = "filmEdit")]
        public async Task<IActionResult> Edit(Guid? id)
        {           
            if (id == null)
            {
                return BadRequest();
            }
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);            
            if (userId == null ) return Forbid();
            
            var film = await _filmService.GetForEditFilmByIdAsync(id.Value);
            if (film == null)
            {
                return NotFound();
            }
            if (userId != film.AutherId) return Forbid();

            ViewData["AutherId"] = userId;
            return View(film);
        }

        // POST: Films/Edit/5        
        [HttpPost("edit/{id:guid}", Name = "filmEditPost")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, FilmCreateEditViewModel film)
        {
            if (id != film.Id)
            {
                return NotFound();
            }
            if (film == null) return BadRequest();
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);            
            if (userId == null) return Forbid();
            if (userId != film.AutherId) return Forbid();

            if (ModelState.IsValid)
            {
                var result = await _filmService.UpdateFilmAsync(id, film, userId);
                _logger.LogInformation($"Film with id={id} was succesfully edited");
                return Redirect(Url.Action(nameof(Details),  new { Id = id })) ;
            }

            ViewData["AutherId"] = userId;
            return View(film);
        }

        // GET: Films/Delete/5
        [Authorize]
        [HttpGet("delete/{id:guid}", Name = "filmDelete")]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return BadRequest();
            }           
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId == null) return Forbid();
            var film = await _filmService.GetShortFilmByIdAsync(id.Value);
            if (userId != film.AutherId) return Forbid();
            return View(film);
        }

        // POST: Films/Delete/5
        [HttpPost("delete/{id:guid}", Name = "filmDeleteConfirmed")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);            
            if (userId == null) return Forbid();

            var film = await _filmService.GetShortFilmByIdAsync(id);
            if (film == null) return NotFound();
            if (userId != film.AutherId) return Forbid();

            var result = await _filmService.RemoveFilmAsync(id, userId);
            _logger.LogInformation($"Film with id={id} was succesfully deleted");
            return RedirectToAction(nameof(Index),new { pageSize=5, pageNumber=1 });
        }


        // GET: Films/AddFile/5
        [Authorize]
        [HttpGet("addposter/{id:guid}", Name = "filmAddPoster")]
        public async Task<IActionResult> AddFile(Guid? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);            
            if (userId == null) return Forbid();
            var film = await _filmService.GetForEditFilmByIdAsync(id.Value);
            if (film == null) return NotFound();
            if (userId != film.AutherId) return Forbid();

            ViewData["AutherId"] = userId;
            return View(film);
        }

        // POST: Films/AddFile/5        
        [HttpPost("addposter/{id:guid}", Name = "filmAddPosterPost")]
        [Authorize]
        [ValidateAntiForgeryToken]       
        public async Task<IActionResult> AddFile(Guid id, IFormFile uploadedFile, string slug)
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);            
            if (userId == null) return Forbid();
            var film = await _filmService.GetForEditFilmByIdAsync(id);
            if (film == null) return NotFound();
            if (userId != film.AutherId) return Forbid();

            if (uploadedFile != null)
            {
                string imagePath = _appEnvironment.WebRootPath + @"\images\";
                string subFolder = slug.Substring(0, 1);
                DirectoryInfo dirInfo = new DirectoryInfo(imagePath + subFolder);
                if (!dirInfo.Exists)
                {
                    dirInfo.Create();
                    _logger.LogInformation($"Folder {imagePath + subFolder} was succesfully created");
                }

                string path = subFolder + @"\" + slug + uploadedFile.FileName.Substring(uploadedFile.FileName.Length-5);                
                using (var fileStream = new FileStream(imagePath + path, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }               
                film.PosterImagePath = path;
                var result = await _filmService.UpdateFilmAsync(id, film, userId);
                _logger.LogInformation($"Poster for film with id={id} was succesfully uploaded");
            } 
            return Redirect(Url.Action(nameof(Details), new { Id = id }));
        }         
    }
}
