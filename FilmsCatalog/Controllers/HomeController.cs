﻿using FilmsCatalog.Models;
using FilmsCatalog.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading.Tasks;

namespace FilmsCatalog.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IFilmService _filmService;
        private readonly IGenreService _genreService;        

        public HomeController(ILogger<HomeController> logger, IFilmService filmService, IGenreService genreService)
        {
            _logger = logger;
            _filmService = filmService;
            _genreService = genreService;
        }

        [HttpGet("/", Name ="home")]
        public async Task<IActionResult> Index()
        {
            var films = await _filmService.GetTopFilmsAsync();


            return View(films);
        }

        [HttpGet("error", Name = "/Home/Error")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
