﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FilmsCatalog.Domain.Models
{
    public class BaseEntity
    {
        [Required]
        public Guid Id { get; set; }

        public DateTime Created { get; set; }

        public BaseEntity() : this(Guid.NewGuid())
        {
        }

        public BaseEntity(Guid guid)
        {
            Id = guid;
            Created = DateTime.Now;
        }

    }
}
