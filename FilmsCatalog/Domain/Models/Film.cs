﻿using System;
using System.Collections.Generic;

namespace FilmsCatalog.Domain.Models
{
    public class Film: BaseEntity    {

        public string Title { get; set; }
        public string Annotation { get; set; }        
        public string Description { get; set; }       
        public int Year { get; set; }       
        public string Director { get; set; }      
        public TimeSpan? Duration { get; set; }       
        public string PosterImagePath { get; set; }        
        public string Slug { get; set; }     
        public string MetaTitle { get; set; }      
        public string MetaDescription { get; set; }       
        public string MetaKeywords { get; set; }       
        public string TrailerUrl { get; set; }

        //relations
        public virtual ICollection<Genre> Genres { get; set; }        
        public string AutherId { get; set; }
        public User Auther { get; set; }
      
        
    }
}
