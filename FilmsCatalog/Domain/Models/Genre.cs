﻿using System.Collections.Generic;

namespace FilmsCatalog.Domain.Models
{
    public class Genre: BaseEntity
    {       
        public string Name { get; set; }
        public virtual ICollection<Film> Films { get; set; }
    }
}
