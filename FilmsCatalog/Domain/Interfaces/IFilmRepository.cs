﻿using FilmsCatalog.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FilmsCatalog.Domain.Interfaces
{
    public interface IFilmRepository : IBaseRepository<Film>
    {

        Task<ICollection<Film>> GetPagedByUserIdAsync(string userId, int pageNumber, int pageSize);
        Task<int> CountByUserId(string userId);
    }
}
