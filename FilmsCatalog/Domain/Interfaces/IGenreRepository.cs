﻿using FilmsCatalog.Domain.Models;

namespace FilmsCatalog.Domain.Interfaces
{
    public interface IGenreRepository : IBaseRepository<Genre>
    {
    }
}