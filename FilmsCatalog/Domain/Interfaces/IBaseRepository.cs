﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FilmsCatalog.Domain.Interfaces
{
    public interface IBaseRepository<TEntity>
        where TEntity : class
    {
        Task<ICollection<TEntity>> GetAllAsync();

        Task<ICollection<TEntity>> GetTopAsync(int count);

        Task<ICollection<TEntity>> GetPagedAsync(int pageNumber, int pageSize);

        Task<int> Count();

        Task<ICollection<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate, int maxItemsAmount = 0);

        Task<TEntity> GetByIdAsync(Guid id);

        Task<Guid> CreateAsync(TEntity entity);

        Task CreateBulkAsync(ICollection<TEntity> entities);

        Task UpdateAsync(TEntity item);

        Task DeleteAsync(TEntity item);

        Task<bool> AnyAsync(Guid id);

        Task DeleteBulkAsync(ICollection<TEntity> items);
    }
}
