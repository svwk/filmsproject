﻿using FilmsCatalog.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FilmsCatalog.Data.EntityConfigurations
{
    public class FilmEntityConfiguration : IEntityTypeConfiguration<Film>
    {
        public void Configure(EntityTypeBuilder<Film> builder)
        {
            builder.ToTable("Films");

            builder.HasKey(r => r.Id);
            builder.HasIndex(r => r.Slug).IsUnique();
            builder.Property(r => r.Title).IsRequired().HasMaxLength(150);
            builder.Property(r => r.Annotation).IsRequired().HasMaxLength(255);
            builder.Property(r => r.Description).IsRequired();
            builder.Property(r => r.Year).IsRequired();
            builder.Property(r => r.Director).HasMaxLength(80);            
            builder.Property(r => r.PosterImagePath).HasMaxLength(300);
            builder.Property(r => r.TrailerUrl).HasMaxLength(300);
            builder.Property(r => r.Slug).IsRequired().HasMaxLength(120);
            builder.Property(r => r.MetaDescription).HasMaxLength(255);
            builder.Property(r => r.MetaKeywords).HasMaxLength(150);
            builder.Property(r => r.MetaTitle).IsRequired().HasMaxLength(100);            

            builder
            .HasMany(c => c.Genres)
            .WithMany(s => s.Films)
            .UsingEntity(j => j.ToTable("FilmGenre"));

            builder
            .HasOne<User>(s => s.Auther)
            .WithMany(g => g.Films)
            .HasForeignKey(s => s.AutherId)
            .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
