﻿using FilmsCatalog.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.SqlClient;

namespace FilmsCatalog.Data
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                using (var context = new ApplicationDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
                {                    
                    try
                    {
                        // Look for any movies.
                        if (context.Genres.Any())
                        {
                            return;   // DB has been seeded
                        }
                    }
                    catch (SqlException ex)
                    {                        
                        if (ex.Number == 4060)
                            //db is not created yet
                            context.Database.Migrate();
                        else throw;
                    }

                    var genres = new[] {
                        new Genre { Name = "Комедия" },
                         new Genre { Name = "Фантастика" },
                         new Genre { Name = "Ужасы" }
                    };

                    context.Genres.AddRange(genres); ;
                    

                    context.Films.AddRange(
                         new Film
                         {
                             Title = "Фарго",
                             Annotation = "Для робкого менеджера по продажам, работающего у собственного тестя, похищение собственной жены — отличная идея.",
                             Description = "Для робкого менеджера по продажам, работающего у собственного тестя, похищение собственной жены — отличная идея. Он нанимает двух преступников, чтобы по результатам разделить с ними крупный выкуп за заложницу. Но дело сразу идет не так, как замышлялось, проливается кровь… Вызов преступникам бросает отважная женщина-полицейский",
                             Year = 1996,
                             Slug = "fargo",
                             AutherId = "3b62472e-4f66-49fa-a20f-e7685b9565d8",
                             Director = "Джоэл Коэн, Итан Коэн",
                             Duration = new TimeSpan(1,57, 0),
                             PosterImagePath = @"f/fargo.jpeg",
                             TrailerUrl = @"https://www.youtube.com/channel/UCdtGvLefetURhWsuhAy6AUA",
                             MetaDescription = "Для робкого менеджера по продажам, работающего у собственного тестя, похищение собственной жены — отличная идея.",
                             MetaTitle = "Фарго",
                             MetaKeywords = "Фарго",
                             Genres = new List<Genre> { genres[0], genres[1] }                             
                         },


                         new Film
                         {
                             Title = "Доказательство смерти",
                             Annotation = "Психопат \"Каскадер\" Майк на своей машине охотится за красивыми девушками.", 
                             Description = "Второй фильм совместного проекта Квентина Тарантино и Роберта Родригеса. Психопат \"Каскадер\" Майк на своей машине охотится за красивыми девушками.Но его последние мишени, похоже, могут дать отпор!",
                             Year = 2007,
                             Slug = "dokazatelstvo-smerti",
                             AutherId = "3b62472e-4f66-49fa-a20f-e7685b9565d8",
                             Director = "Квентин Тарантино",
                             Duration = new TimeSpan(2, 7, 0),
                             PosterImagePath = "d/dokazatelstvo-smerti.jpg",
                             TrailerUrl = @"https://www.youtube.com/watch?v=9JR3Wp3eeUI",
                             MetaDescription = "Психопат \"Каскадер\" Майк на своей машине охотится за красивыми девушками.",
                             MetaTitle = "Доказательство смерти",
                             MetaKeywords = "Доказательство смерти",
                             Genres = new List<Genre> { genres[2] }
                         },

                         new Film
                         {
                             Title = "Страх и ненависть в Лас-Вегасе",
                             Annotation = "Два приятеля едут в Лас-Вегас . Одного из них зовут Рауль Дьюк, он спортивный обозреватель, и в Вегас едет чтобы осветить знаменитую «Минт 400».",
                             Description = "Два приятеля едут в Лас-Вегас . Одного из них зовут Рауль Дьюк, он спортивный обозреватель, и в Вегас едет чтобы осветить знаменитую «Минт 400». Второго зовут Доктор Гонзо. Или что-то вроде того. А еще Доктор Гонзо — адвокат, но какая разница? Да и вокруг творится нечто невообразимое. Родную Неваду не узнать. Только специальные препараты могут спасти от всех этих тварей. Нормальным парням вроде наших героев временами даже как-то неуютно в этом хаосе",
                             Year = 1998,
                             Slug = "strah-i-nenavist-v-las-vegase",
                             AutherId = "3b62472e-4f66-49fa-a20f-e7685b9565d8",
                             Director = "Терри Гиллиам",
                             Duration = new TimeSpan(1,58, 0),
                             PosterImagePath = "s/strah-i-nenavist-v-las-vegase.jpg",
                             TrailerUrl = @"https://www.youtube.com/watch?v=9JR3Wp3eeUI",
                             MetaDescription = "Два приятеля едут в Лас-Вегас . Одного из них зовут Рауль Дьюк, он спортивный обозреватель",
                             MetaTitle = "Страх и ненависть в Лас-Вегасе",
                             MetaKeywords = "Страх и ненависть в Лас-Вегасе",
                             Genres = new List<Genre> { genres[0] }
                         },

                         new Film
                         {
                             Title = "Артист",
                             Annotation = "Голливуд. 1927 год. Немое кино уходит в прошлое, уступая место звуку. Но не все готовы примириться с революцией в киноиндустрии",
                             Description = "Голливуд. 1927 год. Немое кино уходит в прошлое, уступая место звуку. Но не все готовы примириться с революцией в киноиндустрии. Знаменитый актер Джордж Валентайн и слышать не хочет о микрофонах на съемочной площадке. Слава артиста начинает меркнуть. Тем временем тайно влюбленная в Валентайна статистка Пеппи Миллер стремительно набирает популярность, вовсю используя природный шарм и блестящие голосовые данные. И только любовь поможет герою вчерашних дней и звезде нового поколения обрести счастье.",
                             Year = 2011,
                             Slug = "artist",
                             AutherId = "3b62472e-4f66-49fa-a20f-e7685b9565d8",
                             Director = "Мишель Хазанавичус",
                             Duration = new TimeSpan(1, 40, 0),
                             PosterImagePath = "a/artist.jpg",
                             TrailerUrl = @"https://www.youtube.com/watch?v=9JR3Wp3eeUI",
                             MetaDescription = "Голливуд. 1927 год. Немое кино уходит в прошлое, уступая место звуку",
                             MetaTitle = "Артист",
                             MetaKeywords = "Артист",
                             Genres = new List<Genre> { genres[1], genres[2] }
                         },

                         new Film
                         {
                             Title = "Жасмин",
                             Annotation = "В фильме рассказывается история светской львицы (Кейт Бланшетт), муж которой (Алек Болдуин) попадает за решетку из-за своих финансовых махинаций",
                             Description = "В фильме рассказывается история светской львицы (Кейт Бланшетт), муж которой (Алек Болдуин) попадает за решетку из-за своих финансовых махинаций. Дама из высшего общества теперь вынуждена приспосабливаться к жизни без категории VIP и противостоять толпам обманутых инвесторов.",
                             Year = 2013,
                             Slug = "zhasmin",
                             AutherId = "3b62472e-4f66-49fa-a20f-e7685b9565d8",
                             Director = "Вуди Аллен",
                             Duration = new TimeSpan(1, 38, 0),
                             PosterImagePath = "z/zhasmin.jpg",
                             TrailerUrl = @"https://www.youtube.com/watch?v=9JR3Wp3eeUI",
                             MetaDescription = "В фильме рассказывается история светской львицы (Кейт Бланшетт), муж которой",
                             MetaTitle = "Жасмин",
                             MetaKeywords = "Жасмин",
                             Genres = new List<Genre> { genres[1] }
                         },

                         new Film
                         {
                             Title = "Стыд",
                             Annotation = "Драма, рассказывающая о сексуальных похождениях главного героя",
                             Description = "Драма, рассказывающая о сексуальных похождениях главного героя, а также о том, что происходит, когда его своенравная младшая сестра решает к нему присоединиться.",
                             Year = 2011,
                             Slug = "styd",
                             AutherId = "3b62472e-4f66-49fa-a20f-e7685b9565d8",
                             Director = "Стив МакКуин",
                             Duration = new TimeSpan(1,41, 0),
                             PosterImagePath = "s/styd.jpg",
                             TrailerUrl = @"https://www.youtube.com/watch?v=9JR3Wp3eeUI",
                             MetaDescription = "Драма, рассказывающая о сексуальных похождениях главного героя",
                             MetaTitle = "Стыд",
                             MetaKeywords = "Стыд",
                             Genres = new List<Genre> { genres [0], genres [2]}
                         },

                         new Film
                         {
                             Title = "Адаптация",
                             Annotation = "Известный сценарист после прочтения книги знаменитой писательницы Сьюзан Орлин «Похититель орхидей» неудержимо бросается в авантюру",
                             Description = "Известный сценарист после прочтения книги знаменитой писательницы Сьюзан Орлин «Похититель орхидей» неудержимо бросается в авантюру — экранизацию этого опуса. Роман повествует о садоводе, решившем клонировать редкие породы этих удивительных растений. Но на этом пути его ждут одни неприятности",
                             Year = 2002,
                             Slug = "adaptaciya",
                             AutherId = "3b62472e-4f66-49fa-a20f-e7685b9565d8",
                             Director = "Спайк Джонз",
                             Duration = new TimeSpan(1, 54, 0),
                             PosterImagePath = "a/adaptaciya.jpg",
                             TrailerUrl = @"https://www.youtube.com/watch?v=9JR3Wp3eeUI",
                             MetaDescription = "Известный сценарист после прочтения книги знаменитой писательницы Сьюзан Орлин",
                             MetaTitle = "Адаптация",
                             MetaKeywords = "Адаптация",
                             Genres = new List<Genre> { genres[1] }
                         },

                         new Film
                         {
                             Title = "Персонаж",
                             Annotation = "Карен Эйфель заканчивает свой последний роман об одиноком человеке Гарольде Крике. Она даже не подозревает о том, что ее персонаж абсолютно реален",
                             Description = "Карен Эйфель заканчивает свой последний роман об одиноком человеке Гарольде Крике. Она даже не подозревает о том, что ее персонаж абсолютно реален! Настоящий Гарольд Крик умирает от скуки, пока в один прекрасный день не начинает слышать голос Карен, рассказывающий о том, что и как ему следует делать. Жизнь Крика преображается, он начинает общаться с коллегами и ухаживать за женщинами. Настоящее беспокойство овладевает Гарольдом, когда он слышит, что Карен планирует завершить роман его смертью",
                             Year = 2006,
                             Slug = "personazh",
                             AutherId = "3b62472e-4f66-49fa-a20f-e7685b9565d8",
                             Director = "Марк Форстер",
                             Duration = new TimeSpan(1, 0, 0),
                             PosterImagePath = "p/personazh.jpg",
                             TrailerUrl = @"https://www.youtube.com/watch?v=9JR3Wp3eeUI",
                             MetaDescription = "Карен Эйфель заканчивает свой последний роман об одиноком человеке Гарольде Крике",
                             MetaTitle = "Персонаж",
                             MetaKeywords = "Персонаж",
                             Genres = new List<Genre> { genres[0] }
                         },

                         new Film
                         {
                             Title = "Телевикторина",
                             Annotation = "Герберт Стемпл — суперигрок в знаменитом шоу «21», у которого все шансы на огромный выигрыш. Миллионы зрителей с нетерпением ждут финала",
                             Description = "Герберт Стемпл — суперигрок в знаменитом шоу «21», у которого все шансы на огромный выигрыш. Миллионы зрителей с нетерпением ждут финала. Но неожиданно любимец публики уходит со сцены. Вместо него на экране появляется другой участник. Шоу продолжается, ставки снова растут, и в этот момент Стемпл возвращается.На этот раз ему не нужен главный приз.Бывший фаворит собирается разоблачить грандиозный обман и представить зрителям настоящих игроков, тех кто все это время оставался за кадром",
                             Year = 1994,
                             Slug = "televiktorina",
                             AutherId = "3b62472e-4f66-49fa-a20f-e7685b9565d8",
                             Director = "Роберт Редфорд",
                             Duration = new TimeSpan(2, 18, 0),
                             PosterImagePath = "t/televiktorina.jpg",
                             TrailerUrl = @"https://www.youtube.com/watch?v=9JR3Wp3eeUI",
                             MetaDescription = "Герберт Стемпл — суперигрок в знаменитом шоу «21», у которого все шансы",
                             MetaTitle = "Телевикторина",
                             MetaKeywords = "Телевикторина",
                             Genres = new List<Genre> { genres[2] }
                         }
                     ); 
                    context.SaveChanges();
                }
            }

        }
    }
}
