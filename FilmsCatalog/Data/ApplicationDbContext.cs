﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using FilmsCatalog.Domain.Models;
using FilmsCatalog.Data.EntityConfigurations;
using Microsoft.AspNetCore.Identity;


namespace FilmsCatalog.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {

        public DbSet<Film> Films { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<User> ApplicationUsers { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfiguration(new FilmEntityConfiguration());
            modelBuilder.ApplyConfiguration(new GenreEntityConfiguration());
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole
                {
                    Id = "44546e06-8719-4ad8-b88a-f271ae9d6eab",
                    Name = "admin",
                    NormalizedName = "ADMIN"
                },
                new IdentityRole
                {
                    Id = "1818a039-8719-4ad8-b88a-f271ae9d6eab",
                    Name = "user",
                    NormalizedName = "USER"
                }
            );

            modelBuilder.Entity<User>().HasData(new User
            {
                Id = "3b62472e-4f66-49fa-a20f-e7685b9565d8",
                Email = "admin1@mail.com",
                NormalizedEmail = "ADMIN1@MAIL.COM",
                UserName = "admin1",
                FirstName = "admin1",
                LastName = "admin1",
                NormalizedUserName = "ADMIN1",
                PhoneNumber = "+79000000000",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D"),
                PasswordHash = new PasswordHasher<IdentityUser>().HashPassword(null, "123")
            });


            modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = "44546e06-8719-4ad8-b88a-f271ae9d6eab",
                UserId = "3b62472e-4f66-49fa-a20f-e7685b9565d8"
            });
        }

    }
}
