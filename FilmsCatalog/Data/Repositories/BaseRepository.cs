﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using FilmsCatalog.Domain.Models;
using FilmsCatalog.Domain.Interfaces;


namespace FilmsCatalog.Data.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : BaseEntity
    {
        protected ApplicationDbContext Context { get; }

        protected DbSet<TEntity> DbSet { get; }

        public BaseRepository(ApplicationDbContext context)
        {
            Context = context;
            DbSet = Context?.Set<TEntity>();
        }

        public async Task<ICollection<TEntity>> GetAllAsync()
        {
            return await DbSet.AsNoTracking().ToListAsync();
        }

        public async Task<ICollection<TEntity>> GetTopAsync(int count)
        {
            return await DbSet.Take(count).AsNoTracking().ToListAsync();
        }

        public async Task<ICollection<TEntity>> GetPagedAsync(int pageNumber, int pageSize)
        {
            return await DbSet
              .OrderBy(m=>m.Id)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .AsNoTracking()
              .ToListAsync();
        }

        public async Task<int> Count()
        {
            return await DbSet.CountAsync();
        }

        /// <summary>
        /// Collection only for read
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="maxItemsAmount"></param>
        /// <returns></returns>
        public async Task<ICollection<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate,
            int maxItemsAmount = 0)
        {
            var query = DbSet.AsNoTracking().Where(predicate);
            query = maxItemsAmount > 0 ? query.Take(maxItemsAmount) : query;

            return await query.ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<bool> AnyAsync(Guid id)
        {            
            return await DbSet.AnyAsync(e => e.Id == id);
        }

        public async Task<Guid> CreateAsync(TEntity entity)
        {
            await DbSet.AddAsync(entity);

            await SaveChanges();

            return entity.Id;
        }

        public async Task CreateBulkAsync(ICollection<TEntity> entities)
        {
            await DbSet.AddRangeAsync(entities);

            await SaveChanges();
        }

        public async Task UpdateAsync(TEntity item)
        {
            await SaveChanges();
        }

        public async Task DeleteAsync(TEntity item)
        {
            DbSet.Remove(item);

            await SaveChanges();
        }

        public async Task DeleteBulkAsync(ICollection<TEntity> items)
        {
            DbSet.RemoveRange(items);

            await SaveChanges();
        }

        private async Task SaveChanges()
        {
            await Context.SaveChangesAsync();
        }

    }
}