﻿using FilmsCatalog.Domain.Models;
using FilmsCatalog.Domain.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FilmsCatalog.Data.Repositories
{
    public class FilmRepository : BaseRepository<Film>, IFilmRepository
    {
        public FilmRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<ICollection<Film>> GetPagedByUserIdAsync(string userId,int pageNumber, int pageSize)
        {
            if (string.IsNullOrWhiteSpace(userId)) return new List<Film>();
            return await DbSet.Where(x=>x.AutherId==userId)
              .OrderBy(x => x.Id)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .AsNoTracking()
              .ToListAsync();
        }

        public async Task<int> CountByUserId(string userId)
        {
            return await DbSet.Where(x => x.AutherId == userId).CountAsync();
        }
    }
}
