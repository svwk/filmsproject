﻿using FilmsCatalog.Domain.Models;
using FilmsCatalog.Domain.Interfaces;

namespace FilmsCatalog.Data.Repositories
{
    public class GenreRepository : BaseRepository<Genre>, IGenreRepository
    {
        public GenreRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
